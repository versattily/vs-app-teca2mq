﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' As informações gerais sobre um assembly são controladas por
' conjunto de atributos. Altere estes valores de atributo para modificar as informações
' associada a um assembly.

' Revise os valores dos atributos do assembly

<Assembly: AssemblyTitle("Integrador TECAPlus - SAP B1")>
<Assembly: AssemblyDescription("Aplicação de Integração -  two-way OC to MQ and MQ to OC")>
<Assembly: AssemblyCompany("Versattily")>
<Assembly: AssemblyProduct("vs-app-teca2mq")>
<Assembly: AssemblyCopyright("Copyright © Versattily 2018")>
<Assembly: AssemblyTrademark("Versattily")>

<Assembly: ComVisible(False)>

'O GUID a seguir será destinado à ID de typelib se este projeto for exposto para COM
<Assembly: Guid("2ca44754-783d-4eee-b5c2-5956d879cae2")>

' As informações da versão de um assembly consistem nos quatro valores a seguir:
'
'      Versão Principal
'      Versão Secundária 
'      Número da Versão
'      Revisão
'
' É possível especificar todos os valores ou usar como padrão os Números de Build e da Revisão
' utilizando o "*" como mostrado abaixo:
' <Assembly: AssemblyVersion("1.0.*")>

<Assembly: AssemblyVersion("1.0.0.0")>
<Assembly: AssemblyFileVersion("1.0.0.0")>

<Assembly: log4net.Config.XmlConfigurator(ConfigFile:="logger.config", Watch:=True)>