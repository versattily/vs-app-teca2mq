﻿Imports Oracle.ManagedDataAccess.Client
Imports log4net
Imports System.Net
Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.Xml

Module teca2mq
    Public oraconn As New OracleConnection
    Public oraconntrx As New OracleConnection
    Public Ofilial As String
    Public NextCardCode As String
    Public logger As ILog

    Public Sub Main()
        Try
            logger = LogManager.GetLogger("logmng")
            logger.Info("Iniciando TECA2MQ....")
            logger.Info("Versão 1.0.0")
            'Configuração
            Ofilial = My.Settings.oFilial

            oraconn.ConnectionString = "User Id=" & My.Settings.ocuser & ";Password=" & My.Settings.ocpwd & ";Data Source=" & Ofilial & ";DBA Privilege=" & My.Settings.ocdblvl & ";"
            oraconntrx.ConnectionString = "User Id=" & My.Settings.ocuser & ";Password=" & My.Settings.ocpwd & ";Data Source=" & Ofilial & ";DBA Privilege=" & My.Settings.ocdblvl & ";"

            Console.WriteLine("### ###  ##   #   ###   ##  #  ##      ")
            Console.WriteLine("#  #   #     # #    #  #   # # # #     ")
            Console.WriteLine("#  ##  #     ###  ###   #  ### ##      ")
            Console.WriteLine("#  #   #     # #  #      # # # #       ")
            Console.WriteLine("#  ###  ##   # #  ###  ##  # # #       ")
            Console.WriteLine("##################################")
            Console.WriteLine("Release: 201812240912")
            Console.WriteLine("Base:" & Ofilial)
            Mainscope()
        Catch ex As Exception
            logger.Error("Erro fatal:" & ex.Message)
        End Try
    End Sub
    Sub Mainscope()
        Try
            logger.Info("Conectanto do Oracle...")
            '1. Connectar ao BD
            oraconn.Open()
            logger.Info("Conectado!")
            '2.1 Verificar se a View existe no esquema para ser consultada
            'MsgLog("Verificar se a View existe no esquema para ser consultada")
            Dim cmdSchema As New OracleCommand With {
                    .Connection = oraconn,
                    .CommandText = "select count(*) as cnt from all_objects where object_name ='" & My.Settings.ocmainsrc & "'",
                    .CommandType = CommandType.Text
                }
            Dim drSchema As OracleDataReader = cmdSchema.ExecuteReader()
            drSchema.Read()
            Dim cCheck = drSchema.Item("cnt")

            If cCheck > 0 Then
                '2.2 Verificar se há algo para baixar
                'MsgLog("Verificar se há algo para baixar")
                Dim cmd As New OracleCommand With {
                                .Connection = oraconn,
                                .CommandText = "select count(*) as cnt from " & My.Settings.ocmainsrc,
                                .CommandType = CommandType.Text
                            }
                Dim dr As OracleDataReader = cmd.ExecuteReader()
                dr.Read()
                Dim qVals = dr.Item("cnt")

                If qVals > 0 Then
                    logger.Info("Itens a processar: " + qVals.ToString())
                    GetValues()
                Else
                    logger.Info("Sem itens para baixar.")
                End If
            Else
                logger.Info("A View " & My.Settings.ocmainsrc & " não existe na base de dados, não há como continuar, ciclo abortado.")
            End If
            oraconn.Close()

        Catch ex As OracleException
            Select Case ex.Number
                Case 1
                    logger.Error("Erro ao inserir dados que já existem.")
                Case 12560
                    logger.Error("O banco de dados está indisponível.")
                Case Else
                    logger.Error("TECA2MQ - Erro no banco de dados: " + ex.Message.ToString())
            End Select
        Catch ex As Exception
            logger.Error(ex.Message.ToString())
        Finally
            oraconn.Dispose()
        End Try
    End Sub
    Sub GetValues()
        'MsgLog("Obtendo dados da base.")
        Try
            '2.3 Baixa os valores
            Dim cmd As New OracleCommand With {
                .Connection = oraconn,
                .CommandText = "select nvl(TO_CHAR(DTH_REGISTRO,'YYYYMMDD'),'') DTH_REGISTRO,  
                                    NUM_DOCTO_CLIENTE, 
                                    NOM_DOCTO_CLIENTE,
                                    ltrim(rtrim(NUM_DA)) NUM_DA,
                                    nvl(NUM_SEQ_DA,0) NUM_SEQ_DA,
                                    nvl(NUM_DV_DA,0) NUM_DV_DA,
                                    ltrim(rtrim(TIP_NATUREZA_PESSOA)) TIP_NATUREZA_PESSOA,
                                    ltrim(rtrim(NOM_BAIRRO)) NOM_BAIRRO,
                                    ltrim(rtrim(NUM_CEP)) NUM_CEP,
                                    ltrim(rtrim(NOM_CIDADE)) NOM_CIDADE,
                                    ltrim(rtrim(END_LOGRADOURO)) END_LOGRADOURO,
                                    ltrim(rtrim(END_NUM)) END_NUM,
                                    ltrim(rtrim(END_COMPLEMENTO)) END_COMPLEMENTO,
                                    ltrim(rtrim(SIG_UNIDADE_FEDERACAO)) SIG_UNIDADE_FEDERACAO,
                                    ltrim(rtrim(NUM_TELEFONE)) NUM_TELEFONE,
                                    ltrim(rtrim(NOM_END_EMAIL)) NOM_END_EMAIL,    
                                    ltrim(rtrim(COD_ATIVIDADE_ECONOMICA)) COD_ATIVIDADE_ECONOMICA,
                                    ltrim(rtrim(COD_MUNICIPIO_IBGE))  COD_MUNICIPIO_IBGE,
                                    ltrim(rtrim(NUM_INSCR_MUNICIPAL)) NUM_INSCR_MUNICIPAL,
                                    ltrim(rtrim(NUM_INSCR_ESTADUAL)) NUM_INSCR_ESTADUAL,
                                    ltrim(rtrim(SIG_PAIS)) SIG_PAIS,
                                    ltrim(rtrim(TIP_OPERACAO)) TIP_OPERACAO,
                                    ltrim(rtrim(TIP_STATUS_PAGTO)) TIP_STATUS_PAGTO,
                                    ltrim(rtrim(TIP_DOCUMENTO)) TIP_DOCUMENTO,
                                    ltrim(rtrim(COD_BANCO))  COD_BANCO,
                                    ltrim(rtrim(TO_CHAR(DAT_EMISSAO,'YYYYMMDD'))) DAT_EMISSAO,
                                    ltrim(rtrim(DAT_PAGTO)) DAT_PAGTO,
                                    ltrim(rtrim(TO_CHAR(DAT_VENCTO,'YYYYMMDD'))) DAT_VENCTO,
                                    nvl(VAL_TARIFA,0) VAL_TARIFA,
                                    nvl(VAL_ATAERO,0) VAL_ATAERO,
                                    nvl(VAL_RETENCAO,0) VAL_RETENCAO,
                                    nvl(VAL_DEVIDO,0) VAL_DEVIDO,
                                    nvl(VAL_PAGO,0) VAL_PAGO,
                                    nvl(VAL_TOTAL_DA,0) VAL_TOTAL_DA,
                                    ltrim(rtrim(DSC_SERVICO)) DSC_SERVICO,
                                    nvl(NUM_QUANTIDADE,0) NUM_QUANTIDADE,
                                    nvl(VAL_SERVICO,0) VAL_SERVICO,
                                    ltrim(rtrim(TXT_ORIGEM_DA)) TXT_ORIGEM_DA,
                                    ltrim(rtrim(DAT_RECEBIMENTO_CARGA)) DAT_RECEBIMENTO_CARGA,
                                    ltrim(rtrim(DTH_INICIO_SERVICO)) DTH_INICIO_SERVICO,
                                    ltrim(rtrim(DTH_FIM_SERVICO)) DTH_FIM_SERVICO,
                                    ltrim(rtrim(DAT_ENTREGA_CARGA)) DAT_ENTREGA_CARGA
                        from teca." & My.Settings.ocmainsrc,
                .CommandType = CommandType.Text
            }
            Dim dr As OracleDataReader = cmd.ExecuteReader()
            Try
                If dr.HasRows Then
                    Dim cnt As Integer = 0
                    Console.WriteLine("Processsando...")
                    logger.Info("Binding dos dados.")
                    While dr.Read()
                        Dim DA As New DocArrecad With {
                            .Filial = Ofilial.Substring(0, 1),
                            .DTH_REGISTRO = ConvertDBNull(dr.GetString(0)),
                            .NUM_DOCTO_CLIENTE = dr.GetString(1),
                            .NOM_DOCTO_CLIENTE = ConvertDBNull(dr.GetString(2)),
                            .NUM_DA = dr.GetString(3),
                            .NUM_SEQ_DA = dr.GetValue(4),
                            .NUM_DV_DA = dr.GetValue(5),
                            .TIP_NATUREZA_PESSOA = ConvertDBNull(dr.GetValue(6)),
                            .NOM_BAIRRO = ConvertDBNull(dr.GetValue(7)),
                            .NUM_CEP = ConvertDBNull(dr.GetValue(8)),
                            .NOM_CIDADE = ConvertDBNull(dr.GetValue(9)),
                            .END_LOGRADOURO = ConvertDBNull(dr.GetValue(10)),
                            .END_NUMERO = ConvertDBNull(dr.GetValue(11)),'END_NUM
                            .END_COMPLEMENTO = ConvertDBNull(dr.GetValue(12)),
                            .SIG_UNIDADE_FEDERACAO = ConvertDBNull(dr.GetValue(13)),
                            .NUM_TELEFONE = ConvertDBNull(dr.GetValue(14)),
                            .NOM_END_EMAIL = ConvertDBNull(dr.GetValue(15)),
                            .COD_ATIVIDADE_ECONOMICA = ConvertDBNull(dr.GetValue(16)),
                            .COD_MUNICIPIO_IBGE = ConvertDBNull(dr.GetValue(17)),
                            .NUM_INSCR_MUNICIPAL = ConvertDBNull(dr.GetValue(18)),
                            .NUM_INSCR_ESTADUAL = ConvertDBNull(dr.GetValue(19)),
                            .SIG_PAIS = ConvertDBNull(dr.GetValue(20)),
                            .TIP_OPERACAO = ConvertDBNull(dr.GetValue(21)),
                            .TIP_STATUS_PAGTO = ConvertDBNull(dr.GetValue(22)),
                            .TIP_DOCUMENTO = ConvertDBNull(dr.GetValue(23)),
                            .COD_BANCO = ConvertDBNull(dr.GetValue(24)),
                            .DAT_EMISSAO = ConvertDBNull(dr.GetValue(25)),
                            .DAT_PAGTO = ConvertDBNull(dr.GetValue(26)),
                            .DAT_VENCTO = ConvertDBNull(dr.GetValue(27)),
                            .VAL_TARIFA = ConvertDBNull(dr.GetValue(28)),
                            .VAL_ATAERO = ConvertDBNull(dr.GetValue(29)),
                            .VAL_RETENCAO = ConvertDBNull(dr.GetValue(30)),
                            .VAL_DEVIDO = ConvertDBNull(dr.GetValue(31)),
                            .VAL_PAGO = ConvertDBNull(dr.GetValue(32)),
                            .VAL_TOTAL_DA = ConvertDBNull(dr.GetValue(33)),
                            .DSC_SERVICO = ConvertDBNull(dr.GetValue(34)),
                            .NUM_QUANTIDADE = ConvertDBNull(dr.GetValue(35)),
                            .VAL_SERVICO = ConvertDBNull(dr.GetValue(36)),
                            .TXT_ORIGEM_DA = ConvertDBNull(dr.GetValue(37)),
                            .DAT_RECEBIMENTO_CARGA = ConvertDBNull(dr.GetValue(38)),
                            .DTH_INICIO_SERVICO = ConvertDBNull(dr.GetValue(39)),
                            .DTH_FIM_SERVICO = ConvertDBNull(dr.GetValue(40)),
                            .DAT_ENTREGA_CARGA = ConvertDBNull(dr.GetValue(41))
                        }
                        cnt = cnt + 1

                        GetLastBP()
                        If Not CheckBPOnSAP(DA.NUM_DOCTO_CLIENTE, DA.TIP_NATUREZA_PESSOA) Then
                            logger.Info("CNPJ/CPF Existente na Base!")
                        Else
                            logger.Info("CPF/CNPJ não encontrado! Deve se registrar.")
                            AddBP(DA)
                        End If
                        '###################################
                        ' AddJE
                        ' 19/12/2018 - New Logic According Operation Type
                        ' E - Estorno : TBD
                        ' Q - Quitação :  Use AddJE only
                        ' F - A faturar(correntista) : Use AddSO (Opened) only
                        ' G - Estorno correntista : TBD
                        '###################################
                        Select Case (DA.TIP_OPERACAO)
                            Case "E"
                                logger.Info("[ESTORNO] Cenário ainda a ser desenvolvido.")
                            Case "Q"
                                logger.Info("##########[QUITAÇÃO]##########")
                                AddJE(DA)
                                logger.Info("##############################")
                            Case "F"
                                logger.Info("##########[A FATURAR(CORRENTISTA)]##########")
                                AddSO(DA)
                                logger.Info("##############################")
                            Case "G"
                                logger.Info("[ESTORNO CORRENTISTA] Cenário ainda a ser desenvolvido.")
                            Case Else
                                logger.Info("Tipo de operação não documentado. DA.TIP_OPERACAO=" & DA.TIP_OPERACAO)
                        End Select

                        Console.Write("Item:" & cnt.ToString & " / ")
                        Console.WriteLine("DA" & DA.TIP_DOCUMENTO & DA.NUM_DA & DA.NUM_SEQ_DA & "-" & DA.NUM_DV_DA & "  OK!")
                        logger.Info("DA" & DA.TIP_DOCUMENTO & DA.NUM_DA & DA.NUM_SEQ_DA & "-" & DA.NUM_DV_DA & "  OK!")
                    End While
                    Console.Write("Ciclo Finalizado!")
                Else
                    logger.Info("Sem dados para carregar.!")
                End If
            Catch oe As OracleException
                logger.Error("Oracle Error:" & oe.Message.ToString)
            Finally
                dr.Close()
                oraconn.Close()
            End Try
        Catch ex As Exception
            logger.Error("GetValues Error:" & ex.Message.ToString())
        End Try
    End Sub
    Sub updateDAonCloud(ByRef DA As DocArrecad)
        logger.Info("updateDAonCloud():")

        Dim Sql As String

        Dim ocmd As New OracleCommand
        ocmd.CommandType = CommandType.Text
        ocmd.BindByName = True

        If Not oraconntrx.State Then
            oraconntrx.Close()
        End If
        oraconntrx.Open()
        ocmd.Connection = oraconntrx

        Sql = "update teca.INT_DA_CONCESSIONARIA set "
        Sql = Sql & " DTH_PROCESSAMENTO=current_date where NUM_DA='" & DA.NUM_DA & "' and NUM_DV_DA= '" & DA.NUM_DV_DA & "' and NUM_SEQ_DA= '" & DA.NUM_SEQ_DA & "' and TIP_DOCUMENTO= '" & DA.TIP_DOCUMENTO & "'"
        logger.Info("DA Sql=" & Sql)
        Try
            ocmd.CommandText = Sql
            ocmd.ExecuteNonQuery()

            Console.WriteLine("DA" & DA.TIP_DOCUMENTO & DA.NUM_DA & DA.NUM_SEQ_DA & "-" & DA.NUM_DV_DA & " Salvo no Banco!")
            logger.Info("DA" & DA.TIP_DOCUMENTO & DA.NUM_DA & DA.NUM_SEQ_DA & "-" & DA.NUM_DV_DA & "  Salvo no Banco!")
        Catch ex As Exception
            logger.Error("updateDAonCloud Error:" & ex.Message.ToString())
        End Try
    End Sub
    Sub GetLastBP()
        logger.Info("GetLastBP():")
        Try
            Dim msvc As New TECASvc.ipostep_vP0010000123in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient
            Dim NBPIarg As New TECASvc.getNextBPIdType
            Dim NBPIResponse As TECASvc.getNextBPIdResponseType


            With NBPIarg
                .TaxId = "?"
            End With

            NBPIResponse = msvc.ZgetNextBPId(NBPIarg)
            logger.Info("CardCode:" & NBPIResponse.getNextBPIdResult.row.CardCode)
            NextCardCode = NBPIResponse.getNextBPIdResult.row.CardCode

        Catch ex As Exception
            logger.Error(ex.ToString())
        End Try
    End Sub
    Sub AddBP(ByRef DA As DocArrecad)
        logger.Info("AddBP():")
        Try
#Region "AddBP(): Declaration"
            Dim msvc As New TECASvc.ipostep_vP0010000123in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient
            Dim BPAddIarg As New TECASvc.AddBP4TECAType
            Dim BPAddResponse As TECASvc.AddBP4TECAResponseType


            Dim BPFiscalIdTx1 As New TECASvc.AddBP4TECATypeBPFiscalTaxID
            Dim BPFiscalIdTxRow1 As New TECASvc.AddBP4TECATypeBPFiscalTaxIDRow
            Dim BPFiscalIdTxRow2 As New TECASvc.AddBP4TECATypeBPFiscalTaxIDRow

            Dim BPAddr1 As New TECASvc.AddBP4TECATypeBPAddresses
            Dim BPAddrres1 As New TECASvc.AddBP4TECATypeBPAddressesRow
            Dim BPAddrres2 As New TECASvc.AddBP4TECATypeBPAddressesRow

            Dim ContactEmployees1 As New TECASvc.AddBP4TECATypeContactEmployees
            Dim ContactEmployeesRow1 As New TECASvc.AddBP4TECATypeContactEmployeesRow

            Dim newCGC As String
            newCGC = ConvertCGC(DA.NUM_DOCTO_CLIENTE, DA.TIP_NATUREZA_PESSOA)

#End Region
#Region "BPFiscalTaxID"
            '###################################
            ' BPFiscalTaxID
            '###################################
            With BPFiscalIdTxRow1
                .Address = "COBRANÇA"
                .TaxId0 = ""
                .TaxId1 = ""
                .TaxId4 = ""
                .BPCode = NextCardCode
            End With
            With BPFiscalIdTxRow2
                .Address = "ENTREGA"
                .TaxId0 = newCGC
                .TaxId1 = "Isento"
                .TaxId4 = ""
                .BPCode = NextCardCode
            End With

            Dim BPFIT As New List(Of TECASvc.AddBP4TECATypeBPFiscalTaxIDRow) From {
                BPFiscalIdTxRow1,
                BPFiscalIdTxRow2
            }
            With BPFiscalIdTx1
                .row = BPFIT
            End With
            '###################################
#End Region
#Region "BPAddresses"

            '###################################
            ' BPAddresses
            '###################################

            With BPAddrres1
                .AddressName = "COBRANÇA"
                .Street = DA.END_LOGRADOURO
                .Block = DA.NOM_BAIRRO
                .ZipCode = DA.NUM_CEP
                .City = DA.NOM_CIDADE
                If DA.SIG_PAIS <> "" Then
                    .Country = DA.SIG_PAIS.Substring(0, 2)
                Else
                    .Country = DA.SIG_PAIS
                End If
                If DA.SIG_UNIDADE_FEDERACAO <> "" Then
                    .State = DA.SIG_UNIDADE_FEDERACAO.Substring(0, 2)
                Else
                    .State = DA.SIG_UNIDADE_FEDERACAO
                End If
                .AddressType = "B"
                .StreetNo = DA.END_NUMERO
            End With

            With BPAddrres2
                .AddressName = "ENTREGA"
                .Street = DA.END_LOGRADOURO
                .Block = DA.NOM_BAIRRO
                .ZipCode = DA.NUM_CEP
                .City = DA.NOM_CIDADE
                If DA.SIG_PAIS <> "" Then
                    .Country = DA.SIG_PAIS.Substring(0, 2)
                Else
                    .Country = DA.SIG_PAIS
                End If
                If DA.SIG_UNIDADE_FEDERACAO <> "" Then
                    .State = DA.SIG_UNIDADE_FEDERACAO.Substring(0, 2)
                Else
                    .State = DA.SIG_UNIDADE_FEDERACAO
                End If
                .AddressType = "S"
                .StreetNo = DA.END_NUMERO
            End With


            Dim BPADD As New List(Of TECASvc.AddBP4TECATypeBPAddressesRow) From {
                BPAddrres1,
                BPAddrres2
            }
            With BPAddr1
                .row = BPADD
            End With
            '###################################
#End Region
#Region "ContactEmployees"
            '###################################
            ' ContactEmployees
            ' 11/12/2018 - Fix email name before symbol @ for contact Name
            ' 11/12/2018 - No contact to be validated
            '###################################
            With ContactEmployeesRow1
                .CardCode = NextCardCode
                If DA.NOM_END_EMAIL <> "" Then
                    .Name = DA.NOM_END_EMAIL.Substring(0, InStr(DA.NOM_END_EMAIL, "@", CompareMethod.Text) - 1)
                Else
                    .Name = ""
                End If
                .E_Mail = DA.NOM_END_EMAIL
                .Address = ""
            End With
            Dim BPContact As New List(Of TECASvc.AddBP4TECATypeContactEmployeesRow) From {
                ContactEmployeesRow1
            }

            With ContactEmployees1
                .row = BPContact
            End With
            '###################################
#End Region
            '###################################
            ' BPAddIarg
            ' 11/12/2018 - Fix email name before symbol @ for contact Name
            '###################################
            With BPAddIarg
                .CardCode = NextCardCode
                .CardName = DA.NOM_DOCTO_CLIENTE
                .CardType = "C"
                .Phone1 = DA.NUM_TELEFONE
                If DA.NOM_END_EMAIL <> "" Then
                    .ContactPerson = DA.NOM_END_EMAIL.Substring(0, InStr(DA.NOM_END_EMAIL, "@", CompareMethod.Text) - 1)
                Else
                    .ContactPerson = ""
                End If
                .Email = DA.NOM_END_EMAIL
                .WebSite = ""
                .Notes = "Criado via I/F TECAPlus em " & Now().ToString
                .BPFiscalTaxID = BPFiscalIdTx1
                .BPAddresses = BPAddr1
                .ContactEmployees = ContactEmployees1
            End With

            BPAddResponse = msvc.ZAddBP4TECA(BPAddIarg)
            If Not IsNothing(BPAddResponse.AddBP4TECAResult) Then
                logger.Info("BPAddResponse Result:" & BPAddResponse.AddBP4TECAResult)
            Else
                logger.Info("BPAddResponse No Result")
            End If

        Catch ex As FaultException
            Dim detailsMsg As String = String.Empty
            Dim mf As MessageFault = ex.CreateMessageFault
            If mf.HasDetail Then
                Dim ns As XNamespace = "http://url"
                Dim detailedMessage As XElement = mf.GetDetail(Of XElement)()
                Dim messageElement As XElement = detailedMessage.Descendants(ns + "Message").SingleOrDefault
                If messageElement IsNot Nothing Then
                    logger.Error("AddBP Error:" & messageElement.Value.ToString())
                End If
            End If
        End Try
    End Sub
    Sub AddJE(ByRef DA As DocArrecad)
        logger.Info("AddJE():")
        Try
            Dim msvc As New TECASvc.ipostep_vP0010000123in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient
            Dim JEarg As New TECASvc.addJEType

            'JEntry
            Dim JournalEntries1 As New TECASvc.addJETypeJournalEntries
            Dim JournalEntriesRow1 As New TECASvc.addJETypeJournalEntriesRow

            'JEL
            Dim JournalEntriesLines As New TECASvc.addJETypeJournalEntries_Lines
            Dim JournalEntries_Lines1 As New TECASvc.addJETypeJournalEntries_LinesRow
            Dim JournalEntries_Lines2 As New TECASvc.addJETypeJournalEntries_LinesRow

            Dim JEResponse As TECASvc.AddJEResponseType


            'JEL
            With JournalEntries_Lines1
                .Line_ID = 0
                .AccountCode = "1.01.03.01.01"
                .Debit = 0
                .Credit = DA.VAL_TOTAL_DA
                .DueDate = DA.DAT_VENCTO
                .ShortName = NextCardCode
                .ContraAccount = "1.01.01.02.01"
                .TaxDate = DA.DAT_EMISSAO
                .BPLID = DA.Filial
            End With
            With JournalEntries_Lines2
                .Line_ID = 1
                .AccountCode = "1.01.01.02.01"
                .Debit = DA.VAL_TOTAL_DA
                .Credit = 0
                .DueDate = DA.DAT_VENCTO
                .ShortName = "1.01.01.02.01"
                .ContraAccount = NextCardCode
                .TaxDate = DA.DAT_EMISSAO
                .BPLID = DA.Filial
            End With

            Dim JEL As New List(Of TECASvc.addJETypeJournalEntries_LinesRow) From {
                JournalEntries_Lines1,
                JournalEntries_Lines2
            }

            With JournalEntriesLines
                .row = JEL
            End With

            'JE
            With JournalEntriesRow1
                .U_DA = DA.NUM_DA
                .U_DASEQ = DA.NUM_SEQ_DA
                .U_DADV = DA.NUM_DV_DA
                .U_DATipo = DA.TIP_DOCUMENTO
                .U_DARec = Format(Now(), "yyyyMMdd")
                .ReferenceDate = DA.DTH_REGISTRO
                .DueDate = DA.DAT_VENCTO
                .Memo = "DA" & DA.TIP_DOCUMENTO & DA.NUM_DA & DA.NUM_SEQ_DA & "-" & DA.NUM_DV_DA
            End With
            With JournalEntries1
                .row = JournalEntriesRow1
            End With

            With JEarg
                .JournalEntries = JournalEntries1
                .JournalEntries_Lines = JournalEntriesLines
            End With

            JEResponse = msvc.ZAddJournalEntryTECA(JEarg)
            If Not IsNothing(JEResponse.AddJEResult) Then
                logger.Info("JEResponse Result:" & JEResponse.AddJEResult)
                If JEResponse.AddJEResult = 0 Then
                    logger.Info("JE Added with Success!!")
                    'Update TECAPlus DA Status
                    updateDAonCloud(DA)
                End If
            Else
                logger.Info("JEResponse No Result")
            End If
        Catch ex As FaultException
            Dim detailsMsg As String = String.Empty
            Dim mf As MessageFault = ex.CreateMessageFault
            If mf.HasDetail Then
                Dim ns As XNamespace = "http://url"
                Dim detailedMessage As XElement = mf.GetDetail(Of XElement)()
                'Dim messageElement As XElement = detailedMessage.Descendants(ns + "Message").SingleOrDefault
                'If messageElement IsNot Nothing Then
                '    logger.Error("AddJE Error:" & messageElement.Value.ToString())
                'End If
                logger.Error("AddJE Error:" & detailedMessage.Value.ToString())
            End If
            'Catch ex As Exception
            '    logger.Error("AddJE Error:" & ex.ToString())
        End Try
    End Sub
    Sub AddSO(ByRef DA As DocArrecad)
        logger.Info("AddSO():")
#Region "AddSO(): Declaration"
        Dim msvc As New TECASvc.ipostep_vP0010000123in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient
        Dim SOAddIarg As New TECASvc.AddSalesOrder4TECAType
        Dim SOAddResponse As TECASvc.AddSalesOrderResponseType

        Dim SODocument As New TECASvc.AddSalesOrder4TECATypeDocuments
        Dim SODocumentRow As New TECASvc.AddSalesOrder4TECATypeDocumentsRow
        Dim SODocumentLines As New TECASvc.AddSalesOrder4TECATypeDocument_Lines
        Dim SODocumentLinesRow As New TECASvc.AddSalesOrder4TECATypeDocument_LinesRow
#End Region
        Try
#Region "Set Document"
            With SODocumentRow
                .CardCode = NextCardCode
                .Comments = "Criado via I/F TECAPlus em " & Now().ToString
                .DiscountPercent = 0
                .DocCurrency = "R$"
                .DocDate = DA.DAT_EMISSAO
                .DocDueDate = DA.DAT_VENCTO
                .DocTotal = DA.NUM_QUANTIDADE * DA.VAL_SERVICO
                .TaxDate = DA.DAT_PAGTO
                .U_DA = DA.NUM_DA
                .U_DASEQ = DA.NUM_SEQ_DA
                .U_DADV = DA.NUM_DV_DA
                .U_DATipo = DA.TIP_DOCUMENTO
                .U_DARec = Format(Now(), "yyyyMMdd")
            End With
            Dim SOFIT As New List(Of TECASvc.AddSalesOrder4TECATypeDocumentsRow) From {
                SODocumentRow
            }
            With SODocument
                .row = SOFIT
            End With
#End Region
#Region "Document Lines"
            With SODocumentLinesRow
                .ItemCode = "ST00001PL"
                .Quantity = DA.NUM_QUANTIDADE
                .UnitPrice = DA.VAL_SERVICO
            End With
            Dim SOFITRow As New List(Of TECASvc.AddSalesOrder4TECATypeDocument_LinesRow) From {
                SODocumentLinesRow
            }
            With SODocumentLines
                .row = SOFITRow
            End With
#End Region
#Region "Main Tag"
            With SOAddIarg
                .Documents = SODocument
                .Document_Lines = SODocumentLines
                .Branch = DA.Filial
            End With
#End Region
#Region "Response Scope"
            SOAddResponse = msvc.ZAddSalesOrder4TECA(SOAddIarg)
            If Not IsNothing(SOAddResponse.AddSalesOrderResult) Then
                logger.Info("SOAddResponse Result:" & SOAddResponse.AddSalesOrderResult)
                'Update TECAPlus DA Status
                updateDAonCloud(DA)
            Else
                logger.Info("SOAddResponse No Result")
            End If
#End Region
        Catch ex As FaultException
            Dim detailsMsg As String = String.Empty
            Dim mf As MessageFault = ex.CreateMessageFault
            If mf.HasDetail Then
                Dim ns As XNamespace = "http://url"
                Dim detailedMessage As XElement = mf.GetDetail(Of XElement)()
                Dim messageElement As XElement = detailedMessage.Descendants(ns + "Message").SingleOrDefault
                If messageElement IsNot Nothing Then
                    logger.Error("AddSO Error:" & messageElement.Value.ToString())
                End If
            End If
        End Try
    End Sub
    Function CheckBPOnSAP(ByVal cCGC As String, ByVal vPJF As String) As Boolean
        logger.Info("CheckBPOnSAP():")
        'Dim CardCode As String
        'Dim newCGC As String
        Try
            Dim msvc As New TECASvc.ipostep_vP0010000123in_WCSX_comsapb1ivplatformruntime_INB_WS_CALL_SYNC_XPT_INB_WS_CALL_SYNC_XPTipo_procClient
            Dim BPFiscalId As New TECASvc.getBPbyFiscalIdType
            Dim BPFiscalIdResponse As TECASvc.getBPbyFiscalIdResponseType

            'newCGC = ConvertCGC(cCGC, vPJF)
            logger.Info("New CGC():" & cCGC)
            With BPFiscalId
                '.TaxId = newCGC
                .TaxId = cCGC
            End With

            BPFiscalIdResponse = msvc.ZgetBPbyFiscalId(BPFiscalId)

            If IsNothing(BPFiscalIdResponse.getBPbyFiscalIdResult) Then
                logger.Info("FiscalCode Not Found")
                Return True
            Else
                'NextCardCode = BPFiscalIdResponse.getBPbyFiscalIdResult.ToString
                'Dim oXML As New XmlDocument
                'oXML.ImportNode(BPFiscalIdResponse.getBPbyFiscalIdResult, vbTrue)
                Dim root As XmlNode = BPFiscalIdResponse.getBPbyFiscalIdResult(0)
                Dim tValue As String
                If root.HasChildNodes Then
                    Dim i As Integer
                    For i = 0 To root.ChildNodes.Count - 1
                        tValue = root.ChildNodes(i).InnerText
                        If tValue <> vbLf Then
                            NextCardCode = tValue
                        End If
                    Next i
                End If

                logger.Info("Return value [CardCode]:" & NextCardCode)
                logger.Info("FiscalCode Found in SAP")
                Return False
            End If
        Catch ex As Exception
            logger.Error(ex.ToString())
            Return True
        End Try
    End Function
    Function ConvertCGC(ByVal vCGC As String, ByVal vTypeJF As String) As String
        logger.Info("ConvertCGC()")
        logger.Info("vCGC:" & vCGC)
        logger.Info("vTypeJF:" & vTypeJF)
        Try
            If vTypeJF = "F" Then
                'Case CPF
                Const cpfmask As String = "000\.000\.000\-00"
                ConvertCGC = Convert.ToInt64(vCGC).ToString(cpfmask)
                logger.Info("ConvertCGC:" & ConvertCGC)
            ElseIf vTypeJF = "J" Then
                'Case CMPJ
                Const cnpjmask As String = "00\.000\.000\/0000\-00"
                ConvertCGC = Convert.ToInt64(vCGC).ToString(cnpjmask)
                logger.Info("ConvertCGC:" & ConvertCGC)
            Else
                Return vCGC
            End If
        Catch ex As Exception
            Return vCGC
        End Try
    End Function
    Function ConvertDBNull(ByRef cVal) As String
        If IsDBNull(cVal) Then
            Return String.Empty
        Else
            Return cVal
        End If
    End Function
End Module
